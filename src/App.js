import React, {Component} from 'react';
import './App.css';
import {randNumber} from './numbers'
import {randNumber1} from './numbers'
import {randNumber2} from './numbers'
import {randNumber3} from './numbers'
import {randNumber4} from './numbers'
import {randNumber5} from './numbers'
import Person from './Number/Number';


class App extends Component {
    state = {
        randNumber: [
            {number: randNumber},
            {number: randNumber1},
            {number: randNumber2},
            {number: randNumber3},
            {number: randNumber4}
        ]

    };

    increaseNumber = () => {
        const randNumber = this.state.randNumber.map((number) => {
            return {
                number: number.number + randNumber5
            }
        });
        this.setState({randNumber});
    };

    render() {
        return (
            <div className="App">
                <Person number={this.state.randNumber[0].number}/>
                <Person number={this.state.randNumber[1].number}/>
                <Person number={this.state.randNumber[2].number}/>
                <Person number={this.state.randNumber[3].number}/>
                <Person number={this.state.randNumber[4].number}/>
                <div>
                    <button onClick={this.increaseNumber}>New number</button>
                </div>
            </div>
        )
    };
}


export default App;
